from django.shortcuts import render
from rest_framework.pagination import CursorPagination
from rest_framework.generics import ListAPIView

from app.models import Cats
from app.serializers import CatsSerializer
# Create your views here.

class CursorPaginator(CursorPagination):
    page_size=1 #number of items you want per page
    page_size_query_param=page_size
    ordering= "-id"
    
class GetCats(ListAPIView):
    queryset =Cats.objects.all()
    serializer_class =CatsSerializer
    pagination_class= CursorPaginator
 