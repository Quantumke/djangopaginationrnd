from rest_framework import serializers

class CatsSerializer(serializers.Serializer):
    name=serializers.CharField()
    breed = serializers.CharField()